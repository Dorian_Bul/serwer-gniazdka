package serverpkg;

import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.swing.*;

public class Server extends JFrame 
{
    ArrayList clientOutputStreams;
    ArrayList<String> users;
    String haslo = "!@#__!";

    public class ClientHandler implements Runnable
    {
        BufferedReader reader;
        Socket sock;
        PrintWriter client;

        public ClientHandler(Socket clientSocket, PrintWriter user)
        {
            client = user;
            try
            {
                sock = clientSocket;
                InputStreamReader isReader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(isReader);
            }
            catch (Exception ex)
            {
                ta_chat.append("Unexpected error... \n");
            }

        }

        @Override
        public void run()
        {
            String message, connect = "Connect", disconnect = "Disconnect", chat = "Chat" ;
            String[] data;

            try
            {
                while ((message = reader.readLine()) != null)
                {
                    ta_chat.append("Received: " + message + "\n");
                    data = message.split(":");

                    for (String token:data)
                    {
                        ta_chat.append(token + "\n");
                    }

                    if (data[2].equals(connect))
                    {
                        tellEveryone((data[0] + ":" + data[1] + ":" + chat));
                        userAdd(data[0]);
                    }
                    else if (data[2].equals(disconnect))
                    {
                        tellEveryone((data[0] + ":has disconnected." + ":" + chat));
                        userRemove(data[0]);
                    }
                    else if (data[1].equals(haslo))
                    {
                        tellEveryone(message);
                        tellEveryone("Gra:" + "Gratulacje, dobrze!" + ":Chat");
                        haslo = "!@#__!";
                    }
                    else if (data[2].equals(chat))
                    {

                        tellEveryone(message);
                    }
                    else
                    {
                        ta_chat.append("No Conditions were met. \n");
                    }
                }
            }
            catch (Exception ex)
            {
                ta_chat.append("Lost a connection. \n");
                ex.printStackTrace();
                clientOutputStreams.remove(client);
            }
        }
    }

    public Server()
    {
        super( "Gra - Serwer" );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 640);
        setResizable(false);
        setLocation(50,50);
        JPanel panel = new JPanel(null);
        setVisible(true);

        jScrollPane1 = new JScrollPane();
        ta_chat = new JTextArea();
        b_start = new JButton();
        b_end = new JButton();
        b_users = new JButton();
        b_clear = new JButton();

        lb_odpowiedz = new JLabel();
        lb_pytanie = new JLabel();
        tf_chat1 = new JTextField();
        tf_chat2 = new JTextField();
        b_gra = new JButton();

        b_start.setText("START");
        b_start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_startActionPerformed(evt);
            }
        });
        b_end.setText("END");
        b_end.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_endActionPerformed(evt);
            }
        });

        b_users.setText("Online Users");
        b_users.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_usersActionPerformed(evt);
            }
        });

        b_clear.setText("Clear");
        b_clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_clearActionPerformed(evt);
            }
        });

        b_gra.setText("Grajmy!");
        b_gra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_graActionPerformed(evt);
            }
        });
        lb_pytanie.setText("Pytanie:");
        lb_odpowiedz.setText("Odpowiedz:");

        panel.add(ta_chat); panel.add(jScrollPane1);
        panel.add(b_clear); panel.add(b_users); panel.add(b_start); panel.add(b_end);
        panel.add(lb_odpowiedz); panel.add(lb_pytanie); panel.add(tf_chat1); panel.add(tf_chat2); panel.add(b_gra);

        ta_chat.setColumns(20);
        ta_chat.setRows(5);
        jScrollPane1.setViewportView(ta_chat);
        jScrollPane1.setBounds(10,10,780,300);
        jScrollPane1.setBorder(BorderFactory.createLineBorder(Color.black));
        ta_chat.setBounds(10,10,780,300);
        b_start.setBounds(10,320,80,40);
        b_start.setBorder(BorderFactory.createLineBorder(Color.black));
        b_end.setBounds(100, 320, 80,40);
        b_end.setBorder(BorderFactory.createLineBorder(Color.black));
        b_users.setBounds(190, 320, 80,40);
        b_users.setBorder(BorderFactory.createLineBorder(Color.black));
        b_clear.setBounds(280, 320, 80,40);
        b_clear.setBorder(BorderFactory.createLineBorder(Color.black));
        lb_pytanie.setBounds(10, 400, 80, 40);
        lb_pytanie.setBorder(BorderFactory.createLineBorder(Color.black));
        lb_odpowiedz.setBounds(10,450,80,40);
        lb_odpowiedz.setBorder(BorderFactory.createLineBorder(Color.black));
        tf_chat1.setBounds(90,400, 400, 40);
        tf_chat1.setBorder(BorderFactory.createLineBorder(Color.black));
        tf_chat2.setBounds(90,450, 400, 40);
        tf_chat2.setBorder(BorderFactory.createLineBorder(Color.black));
        b_gra.setBounds(10,500,780,100);
        setContentPane(panel);

    }


    private void b_endActionPerformed(java.awt.event.ActionEvent evt) {
        try
        {
            Thread.sleep(500);
        }
        catch(InterruptedException ex) {Thread.currentThread().interrupt();}

        tellEveryone("Server:is stopping and all users will be disconnected.\n:Chat");
        ta_chat.append("Server stopping... \n");

        ta_chat.setText("       _XXX_");
    }

    private void b_startActionPerformed(java.awt.event.ActionEvent evt) {
        Thread starter = new Thread(new ServerStart());
        starter.start();

        ta_chat.append("Start serwera...\n");
    }

    private void b_usersActionPerformed(java.awt.event.ActionEvent evt) {
        ta_chat.append("\n Online users : \n");
        for (String current_user : users)
        {
            ta_chat.append(current_user);
            ta_chat.append("\n");
        }

    }

    private void b_clearActionPerformed(java.awt.event.ActionEvent evt) {
        ta_chat.setText("");
    }

    private void b_graActionPerformed(java.awt.event.ActionEvent evt) {
        String nothing = "";
        if ((tf_chat2.getText()).equals(nothing) && haslo !="!@#__!") {
            tellEveryone("Gra:(Podpowiedź) " + tf_chat1.getText() + ":Chat");
            tf_chat1.setText("");
            tf_chat1.requestFocus();
            tf_chat2.setText("");
            tf_chat2.requestFocus();
        } else if((tf_chat2.getText()).equals(nothing) && haslo =="!@#__!"){
            tellEveryone("Administrator:" + tf_chat1.getText() + ":Chat");

            tf_chat1.setText("");
            tf_chat1.requestFocus();
            tf_chat2.setText("");
            tf_chat2.requestFocus();
        } else {
            tellEveryone("Gra:Czy znacie odpowiedź?:Chat");
            tellEveryone("Gra:" + tf_chat1.getText() + ":Chat");
            haslo = tf_chat2.getText();

            tf_chat1.setText("");
            tf_chat1.requestFocus();
            tf_chat2.setText("");
            tf_chat2.requestFocus();
        }


    }

    public static void main(String args[])
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            @Override
            public void run() {
                new Server().setVisible(true);
            }
        });
    }

    public class ServerStart implements Runnable
    {
        @Override
        public void run()
        {
            clientOutputStreams = new ArrayList();
            users = new ArrayList();

            try
            {
                ServerSocket serverSock = new ServerSocket(9876);

                while (true)
                {
                    Socket clientSock = serverSock.accept();
                    PrintWriter writer = new PrintWriter(clientSock.getOutputStream());
                    clientOutputStreams.add(writer);

                    Thread listener = new Thread(new ClientHandler(clientSock, writer));
                    listener.start();
                    ta_chat.append("Got a connection. \n");
                }
            }
            catch (Exception ex)
            {
                ta_chat.append("Error making a connection. \n");
            }
        }
    }

    public void userAdd (String data)
    {
        String message, add = ": :Connect", done = "Server: :Done", name = data;
        ta_chat.append("Before " + name + " added. \n");
        users.add(name);
        ta_chat.append("After " + name + " added. \n");
        String[] tempList = new String[(users.size())];
        users.toArray(tempList);

        for (String token:tempList)
        {
            message = (token + add);
            tellEveryone(message);
        }
        tellEveryone(done);
    }

    public void userRemove (String data)
    {
        String message, add = ": :Connect", done = "Server: :Done", name = data;
        users.remove(name);
        String[] tempList = new String[(users.size())];
        users.toArray(tempList);

        for (String token:tempList)
        {
            message = (token + add);
            tellEveryone(message);
        }
        tellEveryone(done);
    }

    public void tellEveryone(String message)
    {
        Iterator it = clientOutputStreams.iterator();

        while (it.hasNext())
        {
            try
            {
                PrintWriter writer = (PrintWriter) it.next();
                writer.println(message);
                ta_chat.append("Sending: " + message + "\n");
                writer.flush();
                ta_chat.setCaretPosition(ta_chat.getDocument().getLength());

            }
            catch (Exception ex)
            {
                ta_chat.append("Error telling everyone. \n");
            }
        }
    }


    private JButton b_clear;
    private JButton b_end;
    private JButton b_start;
    private JButton b_users;
    private JButton b_gra;
    private JScrollPane jScrollPane1;
    private JTextArea ta_chat;
    private JLabel lb_pytanie;
    private JLabel lb_odpowiedz;
    private JTextField tf_chat1;
    private JTextField tf_chat2;
}
