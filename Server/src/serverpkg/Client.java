package serverpkg;

import java.io.*;
import java.net.*;
import javax.swing.*;
import java.awt.*;


public class Client extends JFrame 
{
    Socket sock;
    BufferedReader reader;
    PrintWriter writer;
    String username, address = "localhost";
    int port = 9876;
    Boolean isConnected = false;

    public Client()
    {
        super( "Gra - Klient" );
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(800, 600);
        setResizable(false);
        setLocation(50,50);
        JPanel panel = new JPanel(null);
        setVisible(true);

        panel.add(lb_username = new JLabel());
        lb_username.setBounds(10,10,80,40);
        lb_username.setBorder(BorderFactory.createLineBorder(Color.black));
        panel.add(tf_username = new JTextField());
        tf_username.setBounds(90,10,180,40);
        tf_username.setBorder(BorderFactory.createLineBorder(Color.black));
        panel.add(b_connect = new JButton());
        b_connect.setBounds(10,50,80,40);
        b_connect.setBorder(BorderFactory.createLineBorder(Color.black));
        panel.add(b_disconnect = new JButton());
        b_disconnect.setBounds(90,50,80,40);
        b_disconnect.setBorder(BorderFactory.createLineBorder(Color.black));
        panel.add(ta_chat = new JTextArea());
        panel.add(jScrollPane1 = new JScrollPane());
        ta_chat.setColumns(20);
        ta_chat.setRows(5);
        jScrollPane1.setViewportView(ta_chat);
        jScrollPane1.setBounds(10,90,680,410);
        jScrollPane1.setBorder(BorderFactory.createLineBorder(Color.black));
        ta_chat.setBounds(10,90,500,500);
        panel.add(tf_chat = new JTextField());
        tf_chat.setBounds(10,500,600,40);
        tf_chat.setBorder(BorderFactory.createLineBorder(Color.black));
        panel.add(b_send = new JButton());
        b_send.setBounds(610,500,80,40);

        setContentPane(panel);

        lb_username.setText("Nazwa :");
        tf_username.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tf_usernameActionPerformed(evt);
            }
        });
        b_connect.setText("Polacz");
        b_connect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_connectActionPerformed(evt);
            }
        });
        b_disconnect.setText("Rozlacz");
        b_disconnect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_disconnectActionPerformed(evt);
            }
        });
        b_send.setText("WYSLIJ");
        b_send.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                b_sendActionPerformed(evt);
            }
        });
    }

    private void tf_usernameActionPerformed(java.awt.event.ActionEvent evt) {    }
    private void b_connectActionPerformed(java.awt.event.ActionEvent evt) {
        if (isConnected == false)
        {
            username = tf_username.getText();
            if(username.equals("Administrator")) username = "N00b";
            tf_username.setEditable(false);

            try
            {
                sock = new Socket(address, port);
                InputStreamReader streamreader = new InputStreamReader(sock.getInputStream());
                reader = new BufferedReader(streamreader);
                writer = new PrintWriter(sock.getOutputStream());
                writer.println(username + ": dolaczyl(a).:Connect");
                writer.flush();
                isConnected = true;
            }
            catch (Exception ex)
            {
                ta_chat.append("Nie połączono! Spróbuj jeszcze raz. \n");
                tf_username.setEditable(true);
            }

            ListenThread();

        } else if (isConnected == true)
        {
            ta_chat.append("Już tu jesteś. \n");
        }
    }
    private void b_disconnectActionPerformed(java.awt.event.ActionEvent evt) { Disconnect();    }
    private void b_sendActionPerformed(java.awt.event.ActionEvent evt) {
        String nothing = "";
        if ((tf_chat.getText()).equals(nothing)) {
            tf_chat.setText("");
            tf_chat.requestFocus();
        } else {
            try {
                writer.println(username + ":" + tf_chat.getText() + ":" + "Chat");
                writer.flush(); // flushes the buffer
            } catch (Exception ex) {
                ta_chat.append("Błąd wysyłania. \n");
            }
            tf_chat.setText("");
            tf_chat.requestFocus();
        }

        tf_chat.setText("");
        tf_chat.requestFocus();
    }


    public void ListenThread()
    {
        Thread IncomingReader = new Thread(new IncomingReader());
        IncomingReader.start();
    }

    public void Disconnect()
    {
        try
        {
            ta_chat.append("Rozłączono.\n");
            sock.close();
        } catch(Exception ex) {
            ta_chat.append("Nie rozłączono. \n");
        }
        isConnected = false;
        tf_username.setEditable(true);

    }

    public class IncomingReader implements Runnable
    {
        @Override
        public void run()
        {
            String[] data;
            String stream, done = "Done", connect = "Connect", disconnect = "Disconnect", chat = "Chat";

            try
            {
                while ((stream = reader.readLine()) != null)
                {
                    data = stream.split(":");

                    if (data[2].equals(chat))
                    {
                        ta_chat.append(data[0] + ": " + data[1] + "\n");
                        ta_chat.setCaretPosition(ta_chat.getDocument().getLength());
                    }
                    else if (data[2].equals(connect))
                    {
                        ta_chat.removeAll();
                        //userAdd(data[0]);
                    }
                    else if (data[2].equals(disconnect))
                    {
                        //userRemove(data[0]);
                    }
                    else if (data[2].equals(done))
                    {
                        //users.setText("");
                        //writeUsers();
                        //users.clear();
                    }
                }
            }catch(Exception ex) { }
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Client();
            }
        });
    }
    private JButton b_connect;
    private JButton b_disconnect;
    private JButton b_send;
    private JScrollPane jScrollPane1;
    private JLabel lb_username;
    private JTextField tf_username;
    private JTextArea ta_chat;
    private JTextField tf_chat;
}